/*
 Name:		ESP32XyPlotter.ino
 Created:	18-Oct-17 11:33:40 AM
 Author:	sh114203
*/

#include "SDcardUtility.h"
#include <Arduino.h>
#include <WiFi.h>
#include <WiFiMulti.h>

#include <HTTPClient.h>
#include <PubSubClient.h>
#include "SDcardUtility.h"

String url = "http://users.metropolia.fi/~hoangm/gcode.txt";
//String url = "http://users.metropolia.fi/~hoangm/text.txt";

//const char* ssid = "TP-LINK_2CE5E2";
//const char* password = "1213141516171819";
const char* ssid = "a";
const char* password = "123456789";

const char* mqttServer = "iot.eclipse.org";
const int mqttPort = 1883;
const char* inTopic = "ESP32_XYPLOTTER_CML_IN";
const char* outTopicRecv = "ESP32_XYPLOTTER_CML_OUT_RECV"; //to echo back what received
const char* outTopicParse = "ESP32_XYPLOTTER_CML_OUT_PARSE"; //to echo back what received
const char* mqttClientID = "ESP32_XYPLOTTER_UNIQUE_ID_lskjdfklajds923125y235454ver9f4y6";
WiFiClient espClient;
PubSubClient mqttClient(espClient);

enum StatePlotter {
	running, stopping
};

enum StateConnection {
	wifiDisconnect,
	wifiConnectedNoMqtt, //but mqtt is not connected
	mqttConnected  //good to do things
};

StateConnection stateConnection = wifiDisconnect;
StatePlotter statePlotter = stopping;

bool continueSend = false;
bool mqttConnectedshouldPrint = true;

void setup() {
	Serial.begin(115200);
	while (Serial.available()) {
		Serial.read(); //flush serial
	}
	while(!SD.begin()) {
		Serial.println("Card Mount Failed");
		delay(1000);
	}
	WiFi.begin(ssid, password);
	delay(5000);
}

void loop() {
	if (stateConnection == wifiDisconnect) {
		statePlotter = stopping;
		//Serial.println("WIFI DISCONNECT");
		//WiFi.disconnect();
		int waitTime = 5000;
		//WiFi.setAutoReconnect(true);
		while(true){
			if (WiFi.isConnected()) {
				stateConnection = wifiConnectedNoMqtt;
				return;
			}
			WiFi.reconnect();
			delay(waitTime);
		}
	}

	else if (stateConnection == wifiConnectedNoMqtt) {
		statePlotter = stopping;
		//Serial.println("WIFI CONNECTED");
		mqttClient.setServer(mqttServer, mqttPort);
		mqttClient.setCallback(mqttCallback);
		mqttClient.connect(mqttClientID);
		mqttClient.subscribe(inTopic);
		int waitTime = 1500;
		while(true) {
			if (!WiFi.isConnected()) {
				stateConnection = wifiDisconnect;
				return;
			}
			else if (mqttClient.connected()) {
				stateConnection = mqttConnected;
				return;
			}
			delay(waitTime);
		}
	}

	else if (stateConnection == mqttConnected) {
		if (mqttConnectedshouldPrint) {
			//Serial.println("MQTT CONNECTED");
			mqttClient.publish(outTopicParse, "MQTT Connected, ready for cmd");
			mqttConnectedshouldPrint = false;

		}
		if (statePlotter == running && WiFi.isConnected() && mqttClient.connected()) {
			HTTPClient http;
			http.begin(url);
			//start connection and send HTTP header
			int httpCode = http.GET();
			// file found at server
			if (httpCode == HTTP_CODE_OK) {
				int len = http.getSize();// get lenght of document (is -1 when Server sends no Content-Length header)
				uint8_t buff[256] = {0};// create buffer for read
				WiFiClient * stream = http.getStreamPtr();// get tcp stream
				while (Serial.available()) {
					Serial.read(); //flush serial
				}
				deleteFile(SD, "/gcode.txt");
				File fileWrite = SD.open("/gcode.txt", FILE_WRITE);
				//3 min download for 68k line
				while (http.connected() && (len > 0 || len == -1)) {
					size_t size = stream->available();
					if (size) {
						size_t c = stream->readBytesUntil('\0', buff, (size > sizeof(buff)) ? sizeof(buff) : size);// read up to 128 byte
						if (len > 0) {
							len -= c;
						}
						/*Serial.write(buff, c);
						Serial.print('\n');*/
						fileWrite.write(buff, c);
					}
				}
				http.end();//connection closed or file end.\n");*/

				if (!mqttClient.loop()) { //attempt to reconnect mqtt, but should not get blocked forever
					mqttClient.setServer(mqttServer, mqttPort);
					mqttClient.connect(mqttClientID);
					mqttClient.subscribe(inTopic);
					delay(1000);
				}
				mqttClient.publish(outTopicParse, "Finish download file");
				fileWrite.close();
				
				File fileRead = SD.open("/gcode.txt");
				String serialSend;
				String received;
				unsigned long lastMQTTLoopTime = 0;
				unsigned long MQTTLoopInterval = 1000;
				unsigned long serialOkTimeout = 30000;
				while (Serial.available()) {
					Serial.read(); //flush serial
				}
				unsigned long lastWifiRetryTime = 0;
				unsigned long retryInterval = 35000;
				unsigned long waitTime = 5000;
				while (fileRead.available()) {
					if (!WiFi.isConnected() && millis() - lastWifiRetryTime > retryInterval) {
						WiFi.reconnect();
						delay(waitTime); //buy time for reconnection
						lastWifiRetryTime = millis();
					}
					else if (millis() - lastMQTTLoopTime > MQTTLoopInterval) {
						if (!mqttClient.loop()) { //attempt to reconnect mqtt, but should not get blocked
							mqttClient.connect(mqttClientID);
							mqttClient.subscribe(inTopic);
							mqttClient.publish(outTopicParse, "Reconnect to MQTT");
							delay(1500);
							mqttClient.loop();
						}
						lastMQTTLoopTime = millis();
						if (statePlotter == stopping) {
							mqttConnectedshouldPrint = true;
							return;
						}
					}
					serialSend = fileRead.readStringUntil('\n');
					received = "";
					continueSend = false;
					Serial.print(serialSend);
					Serial.print('\n');
					unsigned long startTime = millis();
					while ((statePlotter == running) && (received.indexOf("OK") < 0) 
						&& (received.indexOf("M10 XY 380 310 0.00 0.00 A0 B0 H0 S80 U160 D90") < 0)
						&& (millis() - startTime) < serialOkTimeout 
						&& !continueSend) {
						if (Serial.available()) {
							received = Serial.readStringUntil('\n');
						}
						else if((millis() - lastMQTTLoopTime > MQTTLoopInterval) && mqttClient.loop()) {
							lastMQTTLoopTime = millis();
						}
					}
				}
				mqttClient.publish(outTopicParse, "End sending gcode");
				statePlotter = stopping;
			}
			else {
				mqttClient.publish(outTopicParse, "Bad http respond code");
			}
		}
	}
	if (!WiFi.isConnected()) {
		Serial.println("Lost WiFi Connection");
		stateConnection = wifiDisconnect;
		mqttConnectedshouldPrint = true;
		return;
	}
	else if (!mqttClient.loop()) {
		Serial.println("Lost MQTT Connection");
		stateConnection = wifiConnectedNoMqtt;
		mqttConnectedshouldPrint = true;
		return;
	}
	delay(500);
}

void mqttCallback(char* topic, byte* payload, unsigned int length) {
	char echoBack[256];
	echoBack[255] = '\0';
	char payloadstring[129];
	memset(payloadstring, '\0', sizeof(payloadstring));
	memset(echoBack, '\0', sizeof(echoBack));
	memcpy(payloadstring, payload, length);
	snprintf(echoBack, 255, "Message received at[%s]: %s", topic, payloadstring);
	mqttClient.publish(outTopicRecv, echoBack);
	char* position = nullptr;

	if ((position = strstr((char*)payloadstring, "cmd ")) != nullptr) {
		if (statePlotter == stopping && stateConnection == mqttConnected) {
			Serial.println(position + 4);
		}
		mqttClient.publish(outTopicParse, "Send manual command");
	}
	else if (strcmp(payloadstring, "stop") == 0) {
		statePlotter = stopping;
		mqttClient.publish(outTopicParse, "Set plotter state to stopping");
	}
	else if (strcmp(payloadstring, "start") == 0) {
		statePlotter = running;
		mqttClient.publish(outTopicParse, "Set plotter state to running");
	}
	else if (strcmp(payloadstring, "continue") == 0) {
		continueSend = true;
		mqttClient.publish(outTopicParse, "Continue send next gcode line");
	}
	else {
		mqttClient.publish(outTopicParse, "Unknown command");
	}
}