// 
// 
// 

#include "SDcardUtility.h"

void listDir(fs::FS &fs, const char * dirname, uint8_t levels) {
	Serial.printf("Listing directory: %s\n", dirname);

	File root = fs.open(dirname);
	if (!root) {
		Serial.println("Failed to open directory");
		return;
	}
	if (!root.isDirectory()) {
		Serial.println("Not a directory");
		return;
	}

	File file = root.openNextFile();
	while (file) {
		if (file.isDirectory()) {
			Serial.print("  DIR : ");
			Serial.println(file.name());
			if (levels) {
				listDir(fs, file.name(), levels - 1);
			}
		}
		else {
			Serial.print("  FILE: ");
			Serial.print(file.name());
			Serial.print("  SIZE: ");
			Serial.println(file.size());
		}
		file = root.openNextFile();
	}
}

bool createDir(fs::FS &fs, const char * path) {
	return fs.mkdir(path);
}

bool removeDir(fs::FS &fs, const char * path) {
	fs.rmdir(path);
}

void readFile(fs::FS &fs, const char * path) {
	File file = fs.open(path);
	if (!file) {
		return;
	}
	Serial.print("Read from file: ");
	while (file.available()) {
		Serial.write(file.read());
	}
	file.close();
}

void writeFile(fs::FS &fs, const char * path, const char * message) {
	File file = fs.open(path, FILE_WRITE);
	if (!file) {
		//Serial.println("Failed to open file for writing");
		return;
	}
	file.print(message);
	file.close();
}

void appendFile(fs::FS &fs, const char * path, const char * message) {
	File file = fs.open(path, FILE_APPEND);
	if (!file) {
		return;
	}
	file.print(message); //use this to push to file
	file.close();
}

bool renameFile(fs::FS &fs, const char * path1, const char * path2) {
	return fs.rename(path1, path2);
}

bool deleteFile(fs::FS &fs, const char * path) {
	return fs.remove(path);
}
