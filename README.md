This repository is an utility repository that is used for the xy plotter project:
 1. ESP32XyPlotter: the name is misleading. This is the code for esp32 to communicate and dictate lpc board
 2. GCodeOptimizerFormating: format gcode to contains G0 for compatible with the path optimization program (gcode-optimizer-master.zip)
 3. MdrawGcodeExtractWithArduinoUno: receive gcode from mdraw and echo back to the mDraw terminal
 4. SendGcodeToSerial: send a text file line by line to serial, which should happen to be gcode
 5. Python script to convert image to ascii chars(as a naive attempt to pixellate the image then convert to svg with edge detection, the svg result ends up not very well). But here is an example for that kind of ascii art http://users.metropolia.fi/~hoangm/newFolder/RailgunS_OP1.mp4