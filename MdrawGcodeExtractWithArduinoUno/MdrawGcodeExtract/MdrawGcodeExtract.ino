/*
 Name:		MdrawGcodeExtract.ino
 Created:	18-Oct-17 2:08:41 PM
 Author:	sh114203
*/

void setup() {
	Serial.begin(115200);
	Serial.setTimeout(100000);
}

String str;
void loop() {
	str = "";
	str = Serial.readStringUntil('\n');
	if (str.indexOf("M10") != -1) {
		//Serial.println("M10 XY 380 310 0.00 0.00 A0 B0 H0 S80 U160 D90");
		Serial.println("M10 XY 310 340 0.00 0.00 A0 B0 H0 S80 U160 D90");
		return;
	}
	else {
		Serial.println("OK");
	}
	Serial.println(str);
}