import tkinter.filedialog

textPath = tkinter.filedialog.askopenfilename()
fileIn = open(textPath,"r") #opens file with name of "test.txt"
fileOut = open('restored.txt', 'w')
num_lines = sum(1 for line in open(textPath))
print(num_lines)
#assume the number pen lift is pendown + 1

for i in range (0, num_lines):
    line = fileIn.readline().upper()
    line = line.replace('G0', 'G1')
    fileOut.write(line)
fileIn.close()
fileOut.close()