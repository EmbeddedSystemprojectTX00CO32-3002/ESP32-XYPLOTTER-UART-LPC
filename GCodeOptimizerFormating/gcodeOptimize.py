import tkinter.filedialog

textPath = tkinter.filedialog.askopenfilename()
fileIn = open(textPath,"r") #opens file with name of "test.txt"
fileOut = open("formattedForOptimizing.txt", 'w')
#assume the number pen lift is pendown + 1
changeText = False
num_lines = sum(1 for line in open(textPath))
print(num_lines)
for i in range (0, num_lines):
    line = fileIn.readline()
    if i > 0:
        if(changeText == True):
            line = line.replace('G1','G0')
            changeText = False
        elif(line.find('M1 160') != -1):
            changeText = True
        elif (line.find('M1 90') != - 1):
            changeText = False
    fileOut.write(line)

fileOut.close()