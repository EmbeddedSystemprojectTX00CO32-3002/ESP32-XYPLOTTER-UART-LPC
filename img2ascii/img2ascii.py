import PIL
import shutil
import tkinter.filedialog
from PIL import Image

#WIDTH, HEIGHT = 640, 360  # resize image to this size
#WIDTH, HEIGHT = 800, 290
ratio = 800/260
WIDTH, HEIGHT = int(230*ratio), 230
#ascii_char = list(" .:-=+*#%@")
ascii_char = list("MNFV$I*:. ")
#ascii_char = list("  ..:-=+")
r_cof = 0.0126
g_cof = 0.7152
b_cof = 0.0722

def get_char(r, b, g, alpha = 256):
    if alpha == 0:
        return ' '
    gray = int(r_cof * r + g_cof * g + b_cof *b)
    unit = 256 / len(ascii_char)
    return ascii_char[int(gray/unit)]
    #return ascii_char[len(ascii_char) - 1 - int(gray / unit)]

imgpath = tkinter.filedialog.askopenfilename()
im = Image.open(imgpath)
#WIDTH, HEIGHT = im.size
im = im.resize((WIDTH, HEIGHT), PIL.Image.ANTIALIAS)
#im = im.resize((WIDTH, HEIGHT))
txt = ""
for i in range(HEIGHT):
    if(i > 0): txt = txt + '\n'
    for j in range(WIDTH):
        txt += get_char(*im.getpixel((j,i)))
output_file = "Output" + ".txt"
fo = open(output_file, "w")
fo.write(txt)
fo.close()
